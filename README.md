# myapp

Task: Deploy using Helm-Charts into k8s cluster in the AWS cloud platform.

Things todo:
- Upload Docker image into ACR
- Prepare Helm-Charts
- Develop CI&CD pipeline using gitlab-ci
